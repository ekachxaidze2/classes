// 1)გააკეთოთ character  ების კლასი სადაც შეინახავთ შემდეგ ინფორმაციას: სახელი და როლი (როლი შეიძლება იყოს რამოდენიმენაირი: mage, support, assassin, adc, tank (არ არის აუცილებელი ამ ჩამონათვლის სადმე შენახვა ან ყველა როლის გამოყენება)პ0ო, აქედან support mage  - ც არის.
// 2)ყველა პერსონაჟს სახელთან და როლთან ერთად აქვს armor ისა და damage ის მეთოდები, რომლებიც უბრალოდ ლოგავენ თუ რამდენი არმორი აქვს და რამდენი dmg აქვს პერსონაჟს.
// 3)damage ის შემთხვევაში შეგვიძლია გვააკეთოთ შემოწმება mage, support ზე და სხვა როლებზე, პირველი ორის შემთხვევაში პერსონაჟი magic damage ს აკეთებს, ხოლო სხვა დანარჩენ შემთვევებში attack damage და მეთოდიც ამას ლოგავს.
// 4)mage ის კლასი ასევე მიიღებს magicPowers და აქვს ფრენის მეთოდიც (mage -ები დაფრინავენ კიდეც), რომელიც დალოგავს, რომ ამ მეიჯს შეუძლია ფრენაც. support იგივე მეიჯია, რომელიც მიიღებს heals - ს (თუ რამდენი "ერთეულის" დაჰილვა შეუძლია) ცვლადად და ექნნება  healing მეთოდი რომელიც დალოგავს, რომ ამ გმირს შეუძლია გარკვეული რაოდენობის ერთეულის დაჰილვა.
// 5)ასევე გვყავს adc რომელიც მიიღებს attackdamages ს და აქვს მეთოდი, რომელიც ითვლის მის რეინჯს (მეთოდმა რეინჯის მნიშვნელობა უნდა მიიღოს), ამაზე დაყრდნობით ის ლოგავს ახლო რეინჯიანია პერსონაჟი თუ არა. (პირობითად ვთქვათ, რომ 30 < ახლო რეინჯში ითვლება)

// 6)რაც შეეხება თვითონ პერსონაჟების სახელებს და ყველა იმ მნიშვნელობებს რომლებიც კლასებს უნდა გადასცეთ its up to you. თქვენი ამოცანა კლასების შექმნაში და მათი სტრუქტურის სწორ აგებულებაში მდგომარეობს.

class Character {
    name = "";
    role = "";
    armor = 100;
    damage = 10;
    constructor(name, role) {
        this.name = name;
        this.role = role;
    }
    armor() {
        console.log(this.armor);
    }
    damage() {
        console.log(this.damage);
        if (this.role === "mage" || this.role === "support") {
            console.log("magic damage");
        } else {
            console.log("attact damage");
        }
    }
}
class Mage extends Character {
    magicPower = '';
    constructor(name, role, magicPower) {
        super(name, role);
        this.magicPower = magicPower;
    }
    fly() {
        console.log(`${this.name} can fly`)
    }
}

class Support extends Mage {
    heal = "";
    constructor(name, role, magicPower, heal = 20) {
        super(name, "support", magicPower);
        this.heal = heal;
    }
    healing() {
        console.log(`${this.name} can heal ${this.heal}`)
    }
}
class Adc extends Character {
    attackdamages = '';
    range = '';
    constructor(name, role, range, attackdamages = 15) {
        super(name, role);
        this.attackdamages = attackdamages
        this.range = range
    }
    countRange() {
        if (this.range < 30) {
            console.log(`${this.range} is close`)
        } else {
            console.log(`${this.range} is far`)
        }
    }
}
const katarina = new Mage('katarina', 'mage', "fly");
katarina.fly()
console.log(katarina)

const amumu = new Support("amumu", "support", "magic heal", 45);
amumu.healing()
console.log(amumu)

const kindrer = new Adc("kindrer", "marksman", 21, 15);
kindrer.countRange()
console.log(kindrer)


// const teemo = new Character("teemo", "assassin");
// const gnar = new Character("gnar", "tank");